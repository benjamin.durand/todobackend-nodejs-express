# TodoBackend - NodeJs - Express

Ce projet est une implémentation du [TodoBackend](https://docs.google.com/document/d/1tj_MuyutV6_vzfDpYeEvTuy4n_9ceKPAX8T5lStg2h0/edit), utilisé avec Node.js, Express et TypeScript.

Le projet repose sur une architecture [Clean](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html) :

- **entites** correspond aux entités
- **application** correspond aux cas d'utilisation de l'application
- **controllers** correspond aux adaptateurs communiquant avec la partie application
- **infrastructure** correspondant aux détails de l'application, à savoir la base de données et le système de routing
- **config** correspondant à l'injection de dépendances
- **e2e** correspondant aux tests d'intégrations

## Installation

Exécuter la commande suivante pour installer les dépendances :

`$>npm i`

## Construction

Exécuter la commande suivante pour "build" le projet dans le folder "./dist" :

`$>npm run build`

## Lancement

Exécuter la commande suivante pour lancer l'application :

`$>npm start`

## Tests

Exécuter la commande suivante pour lancer les tests :

`$>npm run test`

Pour lancer les tests en mode "watch", il suffit de préfixer la commande précédente par `:watch` :

`$>npm run test:watch`

Même chose pour lancer le "coverage" du projet en utilisant `:cov` :

`$>npm run test:cov`

## Linting et formatage

Enfin pour "linter" le projet, exécuter la commande suivante :

`$>npm run lint`

Pour formatter le projet, exécuter la commande suivante :

`$>npm run format`
