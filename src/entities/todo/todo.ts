export class Todo {
  constructor(public id: string, public title: string, public order: number, public completed: boolean) {}
}
