import { Todo } from '~/src/entities';

describe('Todo Entity', () => {
  describe('Create', () => {
    it('should create a todo', () => {
      const todo = new Todo('1', ' a title ', 1, false);
      expect(todo.valueOf()).toEqual({
        id: '1',
        title: ' a title ',
        completed: false,
        order: 1,
      });
    });
  });
});
