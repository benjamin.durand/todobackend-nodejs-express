import { Todo } from '~/src/entities';

export type TodoCreate = Omit<Todo, 'id'>;

export type TodoPatch = Partial<Pick<Todo, 'title' | 'order' | 'completed'>>;
