import { Todo } from '~/src/entities';

export interface IRepository<T> {
  getLastOrder: () => Promise<number>;

  create: (todo: Omit<T, 'id'>) => Promise<T>;

  delete: (id: string) => Promise<T | undefined>;

  deleteAll: (completed?: boolean) => Promise<void>;

  fetch: (id: string) => Promise<T | undefined>;

  fetchByOrder: (order: number) => Promise<T | undefined>;

  fetchAll: () => Promise<T[]>;

  patch: (id: string, todoPatch: Partial<T>) => Promise<T | undefined>;
}

export type ITodoRepository = IRepository<Todo>;
