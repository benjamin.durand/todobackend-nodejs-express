import { TodoPatch } from '~/src/application';
import { Todo } from '~/src/entities';

export interface ICreateTodoUseCase {
  exec(title: string): Promise<Todo>;
}

export interface IDeleteTodoUseCase {
  exec(id: string): Promise<Todo | undefined>;
}

export interface IDeleteTodosUseCase {
  exec(completed: boolean): Promise<void>;
}

export interface IFetchTodoUseCase {
  exec(id: string): Promise<Todo | undefined>;
}

export interface IFetchTodosUseCase {
  exec(): Promise<Todo[]>;
}

export interface IPatchTodoUseCase {
  exec(id: string, patch: TodoPatch): Promise<Todo | undefined>;
}

export interface ITodoUseCases {
  createTodoUseCase: ICreateTodoUseCase;
  deleteTodoUseCase: IDeleteTodoUseCase;
  deleteAllTodoUseCase: IDeleteTodosUseCase;
  fetchTodoUseCase: IFetchTodoUseCase;
  fetchAllTodoUseCase: IFetchTodosUseCase;
  patchTodoUseCase: IPatchTodoUseCase;
}
