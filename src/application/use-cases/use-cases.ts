import { ICreateTodoUseCase, IDeleteTodosUseCase, IDeleteTodoUseCase, IFetchTodosUseCase, IFetchTodoUseCase, IPatchTodoUseCase, ITodoUseCases } from '..';

export class TodoUseCases implements ITodoUseCases {
  constructor(
    public createTodoUseCase: ICreateTodoUseCase,
    public deleteTodoUseCase: IDeleteTodoUseCase,
    public deleteAllTodoUseCase: IDeleteTodosUseCase,
    public fetchTodoUseCase: IFetchTodoUseCase,
    public fetchAllTodoUseCase: IFetchTodosUseCase,
    public patchTodoUseCase: IPatchTodoUseCase,
  ) {}
}
