import { IDeleteTodosUseCase, ITodoRepository } from '~/src/application';

export class DeleteTodosUseCase implements IDeleteTodosUseCase {
  constructor(private readonly todoRepository: ITodoRepository) {}

  exec(completed: boolean): Promise<void> {
    return this.todoRepository.deleteAll(completed);
  }
}
