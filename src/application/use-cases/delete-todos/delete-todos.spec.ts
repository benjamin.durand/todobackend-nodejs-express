import { DeleteTodosUseCase, IDeleteTodosUseCase, ITodoRepository } from '~/src/application';

describe('DeleteTodosUseCase', () => {
  let useCase: IDeleteTodosUseCase;
  let repositorySpy: ITodoRepository;

  it.each`
    completed
    ${true}
    ${false}
  `('should delete all todos', async ({ completed }: { completed: boolean }) => {
    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn().mockReturnValue(Promise.resolve()),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new DeleteTodosUseCase(repositorySpy);

    const result = await useCase.exec(completed);

    expect(result).toBeUndefined();
    expect(repositorySpy.deleteAll).toHaveBeenCalledTimes(1);
    expect(repositorySpy.deleteAll).toHaveBeenCalledWith(completed);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it.each`
    completed
    ${true}
    ${false}
  `('should reject promise when deleteAll return an error', async ({ completed }: { completed: boolean }) => {
    const error = new Error("Can't deleteAll");

    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn().mockReturnValue(Promise.reject(error)),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new DeleteTodosUseCase(repositorySpy);

    await expect(useCase.exec(completed)).rejects.toThrow(error);

    expect(repositorySpy.deleteAll).toHaveBeenCalledTimes(1);
    expect(repositorySpy.deleteAll).toHaveBeenCalledWith(completed);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });
});
