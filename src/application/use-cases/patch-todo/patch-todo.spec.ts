import { IPatchTodoUseCase, ITodoRepository, PatchTodoUseCase, TodoPatch } from '~/src/application';
import { Todo } from '~/src/entities';

describe('PatchTodoUseCase', () => {
  let useCase: IPatchTodoUseCase;
  let repositorySpy: ITodoRepository;

  beforeEach(() => {
    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };
  });

  it('should patch a todo', async () => {
    const todo: Todo = new Todo('1', 'a title', 1, false);

    repositorySpy.fetchByOrder = jest.fn().mockReturnValue(Promise.resolve());
    repositorySpy.patch = jest.fn().mockReturnValue(Promise.resolve(todo));

    useCase = new PatchTodoUseCase(repositorySpy);

    const todoId = '1';
    const todoPatch: TodoPatch = { completed: true, order: 2, title: 'summary' };

    const result = await useCase.exec(todoId, todoPatch);

    expect(result).toEqual(todo);
    expect(repositorySpy.fetchByOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetchByOrder).toHaveBeenCalledWith(2);
    expect(repositorySpy.patch).toHaveBeenCalledTimes(1);
    expect(repositorySpy.patch).toHaveBeenCalledWith(todoId, todoPatch);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
  });

  it('should patch a todo', async () => {
    const todo: Todo = new Todo('1', 'a title', 1, false);

    repositorySpy.fetchByOrder = jest.fn().mockReturnValue(Promise.resolve(todo));
    repositorySpy.patch = jest.fn().mockReturnValue(Promise.resolve(todo));

    useCase = new PatchTodoUseCase(repositorySpy);

    const todoId = '1';
    const todoPatch: TodoPatch = { completed: true, order: 2, title: 'summary' };

    const result = await useCase.exec(todoId, todoPatch);

    expect(result).toEqual(todo);
    expect(repositorySpy.fetchByOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetchByOrder).toHaveBeenCalledWith(2);
    expect(repositorySpy.patch).toHaveBeenCalledTimes(1);
    expect(repositorySpy.patch).toHaveBeenCalledWith(todoId, todoPatch);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
  });

  it('should throw exception when conflicting order', async () => {
    const todo: Todo = new Todo('1', 'a title', 1, false);

    repositorySpy.fetchByOrder = jest.fn().mockReturnValue(Promise.resolve({ ...todo, id: 2 }));
    repositorySpy.patch = jest.fn().mockReturnValue(Promise.resolve(todo));

    useCase = new PatchTodoUseCase(repositorySpy);

    const todoId = '1';
    const todoPatch: TodoPatch = { completed: true, order: 2, title: 'summary' };

    await expect(useCase.exec(todoId, todoPatch)).rejects.toThrowError('Conflicting order occurred, order:2 already exists');

    expect(repositorySpy.fetchByOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetchByOrder).toHaveBeenCalledWith(2);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should patch a todo without fetching by order', async () => {
    const todo: Todo = new Todo('1', 'a title', 1, false);

    repositorySpy.fetchByOrder = jest.fn().mockReturnValue(Promise.resolve());
    repositorySpy.patch = jest.fn().mockReturnValue(Promise.resolve(todo));

    useCase = new PatchTodoUseCase(repositorySpy);

    const todoId = '1';
    const todoPatch: TodoPatch = { completed: true, title: 'summary' };

    const result = await useCase.exec(todoId, todoPatch);

    expect(result).toEqual(todo);
    expect(repositorySpy.patch).toHaveBeenCalledTimes(1);
    expect(repositorySpy.patch).toHaveBeenCalledWith(todoId, todoPatch);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchByOrder).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
  });

  it('should return an error when fetchByOrder rejects', async () => {
    const error = new Error("Can't fetch todo by order");

    repositorySpy.fetchByOrder = jest.fn().mockReturnValue(Promise.reject(error));

    useCase = new PatchTodoUseCase(repositorySpy);

    const todoId = '1';
    const todoPatch: TodoPatch = { completed: true, order: 2, title: 'summary' };

    await expect(useCase.exec(todoId, todoPatch)).rejects.toThrow(error);

    expect(repositorySpy.fetchByOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetchByOrder).toHaveBeenCalledWith(2);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should return an error when patch rejects', async () => {
    const error = new Error("Can't patch todo");

    repositorySpy.patch = jest.fn().mockReturnValue(Promise.reject(error));

    useCase = new PatchTodoUseCase(repositorySpy);

    const todoId = '1';
    const todoPatch: TodoPatch = { completed: true, order: 2, title: 'summary' };

    await expect(useCase.exec(todoId, todoPatch)).rejects.toThrow(error);

    expect(repositorySpy.fetchByOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetchByOrder).toHaveBeenCalledWith(2);
    expect(repositorySpy.patch).toHaveBeenCalledTimes(1);
    expect(repositorySpy.patch).toHaveBeenCalledWith(todoId, todoPatch);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
  });
});
