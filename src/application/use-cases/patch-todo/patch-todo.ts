import { ConflictingOrderException, IPatchTodoUseCase, ITodoRepository, TodoPatch } from '~/src/application';
import { Todo } from '~/src/entities';

export class PatchTodoUseCase implements IPatchTodoUseCase {
  constructor(private readonly todoRepository: ITodoRepository) {}

  async exec(id: string, patch: TodoPatch): Promise<Todo | undefined> {
    if (patch.order) {
      const todo = await this.todoRepository.fetchByOrder(patch.order);
      if (todo && todo.id !== id) {
        throw new ConflictingOrderException(`Conflicting order occurred, order:${patch.order} already exists`);
      }
    }
    return this.todoRepository.patch(id, patch);
  }
}
