import { FetchTodoUseCase, IFetchTodoUseCase, ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';

describe('FetchTodoUseCase', () => {
  let useCase: IFetchTodoUseCase;
  let repositorySpy: ITodoRepository;

  it('should fetch a todo', async () => {
    const todo: Todo = new Todo('1', 'a title', 1, false);

    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn().mockReturnValue(Promise.resolve(todo)),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new FetchTodoUseCase(repositorySpy);

    const todoId = '1';

    const result = await useCase.exec(todoId);

    expect(result).toEqual(todo);
    expect(repositorySpy.fetch).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetch).toHaveBeenCalledWith(todoId);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should return undefined when no todo', async () => {
    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn().mockReturnValue(Promise.resolve(undefined)),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new FetchTodoUseCase(repositorySpy);

    const todoId = '1';

    const result = await useCase.exec(todoId);

    expect(result).toBeUndefined();
    expect(repositorySpy.fetch).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetch).toHaveBeenCalledWith(todoId);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should reject promise when fetch return an error', async () => {
    const error = new Error("Can't fetch todo");

    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn().mockReturnValue(Promise.reject(error)),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new FetchTodoUseCase(repositorySpy);

    const todoId = '1';

    await expect(useCase.exec(todoId)).rejects.toThrow(error);

    expect(repositorySpy.fetch).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetch).toHaveBeenCalledWith(todoId);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });
});
