import { IFetchTodoUseCase, ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';

export class FetchTodoUseCase implements IFetchTodoUseCase {
  constructor(private readonly todoRepository: ITodoRepository) {}

  exec(id: string): Promise<Todo | undefined> {
    return this.todoRepository.fetch(id);
  }
}
