import { DeleteTodoUseCase, IDeleteTodoUseCase, ITodoRepository } from '~/src/application';

describe('DeleteTodoUseCase', () => {
  let useCase: IDeleteTodoUseCase;
  let repositorySpy: ITodoRepository;

  it('should delete a todo', async () => {
    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn().mockReturnValue(Promise.resolve()),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new DeleteTodoUseCase(repositorySpy);

    const todoId = '1';

    const result = await useCase.exec(todoId);

    expect(result).toBeUndefined();
    expect(repositorySpy.delete).toHaveBeenCalledTimes(1);
    expect(repositorySpy.delete).toHaveBeenCalledWith(todoId);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should reject promise when delete return an error', async () => {
    const error = new Error("Can't delete todo");

    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn().mockReturnValue(Promise.reject(error)),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new DeleteTodoUseCase(repositorySpy);

    const todoId = '1';

    await expect(useCase.exec(todoId)).rejects.toThrow(error);

    expect(repositorySpy.delete).toHaveBeenCalledTimes(1);
    expect(repositorySpy.delete).toHaveBeenCalledWith(todoId);

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });
});
