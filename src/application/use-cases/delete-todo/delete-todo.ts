import { IDeleteTodoUseCase, ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';

export class DeleteTodoUseCase implements IDeleteTodoUseCase {
  constructor(private readonly todoRepository: ITodoRepository) {}

  exec(id: string): Promise<Todo | undefined> {
    return this.todoRepository.delete(id);
  }
}
