import { ICreateTodoUseCase, ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';

export class CreateTodoUseCase implements ICreateTodoUseCase {
  constructor(private readonly todoRepository: ITodoRepository) {}

  async exec(title: string): Promise<Todo> {
    const order = (await this.todoRepository.getLastOrder()) + 1;
    return this.todoRepository.create({ title, order, completed: false });
  }
}
