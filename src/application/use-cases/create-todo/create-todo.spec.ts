import { CreateTodoUseCase, ICreateTodoUseCase, ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';

describe('CreateTodoUseCase', () => {
  let useCase: ICreateTodoUseCase;
  let repositorySpy: ITodoRepository;

  it('should create a todo', async () => {
    const todo: Todo = new Todo('1', 'a title', 1, false);

    repositorySpy = {
      getLastOrder: jest.fn().mockReturnValue(Promise.resolve(0)),
      create: jest.fn().mockReturnValue(Promise.resolve(todo)),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    const getLastOrderSpy = jest.spyOn(repositorySpy, 'getLastOrder');
    const createSpy = jest.spyOn(repositorySpy, 'create');

    useCase = new CreateTodoUseCase(repositorySpy);

    const title = 'a title';

    const result = await useCase.exec(title);

    expect(result).toEqual(todo);
    expect(repositorySpy.getLastOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.getLastOrder).toHaveBeenCalledWith();

    expect(repositorySpy.create).toHaveBeenCalledTimes(1);
    expect(repositorySpy.create).toHaveBeenCalledWith({ title, order: 1, completed: false });

    expect(getLastOrderSpy.mock.invocationCallOrder[0]).toBeLessThan(createSpy.mock.invocationCallOrder[0]);

    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should reject promise when getLastOrder return an error', async () => {
    const error = new Error("Can't get last order");

    repositorySpy = {
      getLastOrder: jest.fn().mockReturnValue(Promise.reject(error)),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    useCase = new CreateTodoUseCase(repositorySpy);

    const title = 'a title';

    await expect(useCase.exec(title)).rejects.toThrow(error);

    expect(repositorySpy.getLastOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.getLastOrder).toHaveBeenCalledWith();

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should reject promise when create return an error', async () => {
    const error = new Error("Can't create todo");

    repositorySpy = {
      getLastOrder: jest.fn().mockReturnValue(Promise.resolve(0)),
      create: jest.fn().mockReturnValue(Promise.reject(error)),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn(),
      patch: jest.fn(),
    };

    const getLastOrderSpy = jest.spyOn(repositorySpy, 'getLastOrder');
    const createSpy = jest.spyOn(repositorySpy, 'create');

    useCase = new CreateTodoUseCase(repositorySpy);

    const title = 'a title';

    await expect(useCase.exec(title)).rejects.toThrow(error);

    expect(repositorySpy.getLastOrder).toHaveBeenCalledTimes(1);
    expect(repositorySpy.getLastOrder).toHaveBeenCalledWith();

    expect(repositorySpy.create).toHaveBeenCalledTimes(1);
    expect(repositorySpy.create).toHaveBeenCalledWith({ title, order: 1, completed: false });

    expect(getLastOrderSpy.mock.invocationCallOrder[0]).toBeLessThan(createSpy.mock.invocationCallOrder[0]);

    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.fetchAll).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });
});
