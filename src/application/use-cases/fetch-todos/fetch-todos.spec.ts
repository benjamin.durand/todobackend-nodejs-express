import { FetchTodosUseCase, IFetchTodosUseCase, ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';

describe('FetchTodosUseCase', () => {
  let useCase: IFetchTodosUseCase;
  let repositorySpy: ITodoRepository;

  it('should fetch todos ordered', async () => {
    const todos: Todo[] = [new Todo('2', 'another title', 2, false), new Todo('3', 'title', 3, false), new Todo('1', 'a title', 1, false)];

    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn().mockReturnValue(Promise.resolve(todos)),
      patch: jest.fn(),
    };

    useCase = new FetchTodosUseCase(repositorySpy);

    const result = await useCase.exec();

    expect(result).toEqual(todos);
    expect(result.map((todo) => todo.id)).toEqual(['1', '2', '3']);
    expect(repositorySpy.fetchAll).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetchAll).toHaveBeenCalledWith();

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });

  it('should reject promise when fetchAll return an error', async () => {
    const error = new Error("Can't fetch todos");

    repositorySpy = {
      getLastOrder: jest.fn(),
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchByOrder: jest.fn(),
      fetchAll: jest.fn().mockReturnValue(Promise.reject(error)),
      patch: jest.fn(),
    };

    useCase = new FetchTodosUseCase(repositorySpy);

    await expect(useCase.exec()).rejects.toThrow(error);

    expect(repositorySpy.fetchAll).toHaveBeenCalledTimes(1);
    expect(repositorySpy.fetchAll).toHaveBeenCalledWith();

    expect(repositorySpy.create).not.toHaveBeenCalled();
    expect(repositorySpy.delete).not.toHaveBeenCalled();
    expect(repositorySpy.deleteAll).not.toHaveBeenCalled();
    expect(repositorySpy.fetch).not.toHaveBeenCalled();
    expect(repositorySpy.patch).not.toHaveBeenCalled();
  });
});
