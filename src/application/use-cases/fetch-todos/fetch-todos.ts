import { IFetchTodosUseCase, ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';

export class FetchTodosUseCase implements IFetchTodosUseCase {
  constructor(private readonly todoRepository: ITodoRepository) {}

  async exec(): Promise<Todo[]> {
    const todos = await this.todoRepository.fetchAll();
    return todos.sort((a, b) => a.order - b.order);
  }
}
