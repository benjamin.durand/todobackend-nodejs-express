import { config } from 'dotenv';
import express from 'express';
import { Express } from 'express-serve-static-core';
import http from 'http';
import path from 'path';
import request, { agent } from 'supertest';
import { container } from '~/src/config';

describe('todo-router', () => {
  config({ path: path.join(__dirname, '.env') });
  const port = process.env.PORT || 4050;

  let app: Express;
  let httpServer: http.Server;
  let request: request.SuperAgentTest;

  beforeEach(() => {
    app = express();
    app.use(express.json());
    app.use('/todos', container.cradle.todoRouter.router);

    httpServer = app.listen(port);
    request = agent(httpServer);
  });

  afterEach((done) => {
    httpServer.close(done);
    container.dispose();
  });

  describe('POST on route /', () => {
    let req: request.Test;

    beforeEach(() => {
      req = request.post('/todos').set('Accept', 'application/json');
    });

    it('should get the created todo', async () => {
      await req
        .send({ title: 'a title' })
        .expect('Content-Type', /json/)
        .expect(201)
        .expect((res) => {
          const uuid = res.body.id;
          expect(uuid).toEqual(expect.any(String));
          expect(res.body).toEqual({ id: uuid, title: 'a title', completed: false, order: 1, url: `http://127.0.0.1:${port}/todos/${uuid}` });
        });
    });

    it.each`
      title
      ${''}
      ${'  '}
    `('should get 400 when invalid title', async ({ title }: { title: string }) => {
      await req.send({ title }).expect('Content-Type', 'application/json; charset=utf-8').expect(400, '"title must not be blank"');
    });
  });

  describe('GET on route /', () => {
    let req: request.Test;

    beforeEach(() => {
      req = request.get('/todos').set('Accept', 'application/json');
    });

    it('should get no todo', async () => {
      await req.send().expect('Content-Type', /json/).expect(200, []);
    });

    it('should get todos', async () => {
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'a title' });
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'another title' });
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'summary' });

      await req
        .send()
        .expect('Content-Type', /json/)
        .expect(200)
        .expect((res) => {
          const [uuid1, uuid2, uuid3] = res.body.map((item: { id: string }) => item.id);
          expect([uuid1, uuid2, uuid3]).toEqual([expect.any(String), expect.any(String), expect.any(String)]);
          expect(res.body).toEqual([
            { id: uuid1, title: 'a title', completed: false, order: 1, url: `http://127.0.0.1:${port}/todos/${uuid1}` },
            { id: uuid2, title: 'another title', completed: false, order: 2, url: `http://127.0.0.1:${port}/todos/${uuid2}` },
            { id: uuid3, title: 'summary', completed: false, order: 3, url: `http://127.0.0.1:${port}/todos/${uuid3}` },
          ]);
        });
    });
  });

  describe('DELETE on route /', () => {
    let req: request.Test;

    beforeEach(() => {
      req = request.delete('/todos').set('Accept', 'application/json');
    });

    it('should delete all todos', async () => {
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'a title' });
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'another title' });
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'summary' });

      let todos = await request.get('/todos').set('Accept', 'application/json').send().expect(200);
      expect(todos.body).toHaveLength(3);

      await req.send().expect(204);

      todos = await request.get('/todos').set('Accept', 'application/json').send().expect(200);
      expect(todos.body).toHaveLength(0);
    });

    it('should delete all completed todos', async () => {
      let uuid = '';
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'a title' });
      await request
        .post('/todos')
        .set('Accept', 'application/json')
        .send({ title: 'another title' })
        .expect((res) => (uuid = res.body.id));
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'summary' });

      await request
        .patch(`/todos/${uuid}`)
        .set('Accept', 'application/json')
        .send({ completed: true })
        .expect(200)
        .expect((res) => {
          const uuid = res.body.id;
          expect(uuid).toEqual(expect.any(String));
          expect(res.body).toEqual({ id: uuid, title: 'another title', completed: true, order: 2, url: `http://127.0.0.1:${port}/todos/${uuid}` });
        });

      await request
        .get('/todos')
        .set('Accept', 'application/json')
        .send()
        .expect(200)
        .expect((res) => {
          const [uuid1, uuid2, uuid3] = res.body.map((item: { id: string }) => item.id);
          expect([uuid1, uuid2, uuid3]).toEqual([expect.any(String), expect.any(String), expect.any(String)]);
          expect(res.body).toEqual([
            { id: uuid1, title: 'a title', completed: false, order: 1, url: `http://127.0.0.1:${port}/todos/${uuid1}` },
            { id: uuid2, title: 'another title', completed: true, order: 2, url: `http://127.0.0.1:${port}/todos/${uuid2}` },
            { id: uuid3, title: 'summary', completed: false, order: 3, url: `http://127.0.0.1:${port}/todos/${uuid3}` },
          ]);
        });

      await req.query({ completed: true }).send().expect(204);

      await request
        .get('/todos')
        .set('Accept', 'application/json')
        .send()
        .expect(200)
        .expect((res) => {
          const [uuid1, uuid2] = res.body.map((item: { id: string }) => item.id);
          expect([uuid1, uuid2]).toEqual([expect.any(String), expect.any(String)]);
          expect(res.body).toEqual([
            { id: uuid1, title: 'a title', completed: false, order: 1, url: `http://127.0.0.1:${port}/todos/${uuid1}` },
            { id: uuid2, title: 'summary', completed: false, order: 3, url: `http://127.0.0.1:${port}/todos/${uuid2}` },
          ]);
        });
    });
  });

  describe('GET on route /:id', () => {
    let req: () => request.Test;
    let id: string;

    beforeEach(() => {
      req = () => request.get(`/todos/${id}`).set('Accept', 'application/json');
    });

    it('should get todo', async () => {
      await request
        .post('/todos')
        .set('Accept', 'application/json')
        .send({ title: 'a title' })
        .expect((res) => (id = res.body.id));

      await req()
        .send()
        .expect(200)
        .expect((res) => {
          const uuid = res.body.id;
          expect(uuid).toEqual(expect.any(String));
          expect(res.body).toEqual({ id: uuid, title: 'a title', completed: false, order: 1, url: `http://127.0.0.1:${port}/todos/${uuid}` });
        });
    });

    it('should get 404 when unknow id', async () => {
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'a title' });

      id = '2';
      await req().send().expect(404, 'Not Found');
    });
  });

  describe('PATCH on route /:id', () => {
    let req: () => request.Test;
    let id: string;

    beforeEach(() => {
      req = () => request.patch(`/todos/${id}`).set('Accept', 'application/json');
    });

    it('should patch todo', async () => {
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'title 1' });
      await request
        .post('/todos')
        .set('Accept', 'application/json')
        .send({ title: 'title 2' })
        .expect((res) => (id = res.body.id));
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'title 3' });

      // Premier patch
      await req()
        .send({ title: 'title 2 updated', completed: true, order: 4 })
        .expect(200)
        .expect((res) => {
          const uuid = res.body.id;
          expect(uuid).toEqual(expect.any(String));
          expect(res.body).toEqual({ id: uuid, title: 'title 2 updated', completed: true, order: 4, url: `http://127.0.0.1:${port}/todos/${uuid}` });
        });

      await request
        .get('/todos')
        .set('Accept', 'application/json')
        .send()
        .expect(200)
        .expect((res) => {
          const [uuid1, uuid2, uuid3] = res.body.map((item: { id: string }) => item.id);
          expect([uuid1, uuid2, uuid3]).toEqual([expect.any(String), expect.any(String), expect.any(String)]);
          expect(res.body).toEqual([
            { id: uuid1, title: 'title 1', completed: false, order: 1, url: `http://127.0.0.1:${port}/todos/${uuid1}` },
            { id: uuid2, title: 'title 3', completed: false, order: 3, url: `http://127.0.0.1:${port}/todos/${uuid2}` },
            { id: uuid3, title: 'title 2 updated', completed: true, order: 4, url: `http://127.0.0.1:${port}/todos/${uuid3}` },
          ]);
        });

      // Second patch
      await req()
        .send({ order: 2 })
        .expect(200)
        .expect((res) => {
          const uuid = res.body.id;
          expect(uuid).toEqual(expect.any(String));
          expect(res.body).toEqual({ id: uuid, title: 'title 2 updated', completed: true, order: 2, url: `http://127.0.0.1:${port}/todos/${uuid}` });
        });

      await request
        .get('/todos')
        .set('Accept', 'application/json')
        .send()
        .expect(200)
        .expect((res) => {
          const [uuid1, uuid2, uuid3] = res.body.map((item: { id: string }) => item.id);
          expect([uuid1, uuid2, uuid3]).toEqual([expect.any(String), expect.any(String), expect.any(String)]);
          expect(res.body).toEqual([
            { id: uuid1, title: 'title 1', completed: false, order: 1, url: `http://127.0.0.1:${port}/todos/${uuid1}` },
            { id: uuid2, title: 'title 2 updated', completed: true, order: 2, url: `http://127.0.0.1:${port}/todos/${uuid2}` },
            { id: uuid3, title: 'title 3', completed: false, order: 3, url: `http://127.0.0.1:${port}/todos/${uuid3}` },
          ]);
        });
    });

    it('should get 404 when unknow id', async () => {
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'a title' });

      id = '2';
      await req().send({ title: 'title updated' }).expect(404, 'Not Found');
    });

    it('should get 409 when conflicting order', async () => {
      await request
        .post('/todos')
        .set('Accept', 'application/json')
        .send({ title: 'title 1' })
        .expect((res) => (id = res.body.id));
      await request.post('/todos').set('Accept', 'application/json').send({ title: 'title 2' });

      await req().send({ order: 2 }).expect(409, '"Conflicting order occurred, order:2 already exists"');
    });

    it('should get 400 when invalid order', async () => {
      await request
        .post('/todos')
        .set('Accept', 'application/json')
        .send({ title: 'title 1' })
        .expect((res) => (id = res.body.id));

      await req().send({ order: -1 }).expect(400, '"order must not be less than 1"');
    });

    it('should get 400 when invalid title', async () => {
      await request
        .post('/todos')
        .set('Accept', 'application/json')
        .send({ title: 'title 1' })
        .expect((res) => (id = res.body.id));

      await req().send({ title: '' }).expect(400, '"title must not be blank"');
      await req().send({ title: '  ' }).expect(400, '"title must not be blank"');
    });
  });

  describe('DELETE on route /:id', () => {
    let req: () => request.Test;
    let id: string;

    beforeEach(() => {
      req = () => request.delete(`/todos/${id}`).set('Accept', 'application/json');
    });

    it('should delete todo', async () => {
      await request
        .post('/todos')
        .set('Accept', 'application/json')
        .send({ title: 'title 1' })
        .expect((res) => (id = res.body.id));

      await req().send().expect(204);

      await request.get('/todos').set('Accept', 'application/json').send().expect(200, []);
    });
  });
});
