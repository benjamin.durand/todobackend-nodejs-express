/* eslint-disable @typescript-eslint/no-explicit-any */
import { ITodoRepository } from '~/src/application';
import { Todo } from '~/src/entities';
import { InMemoryTodoRepository, IUuidGenerator } from '~/src/infrastructure/repository';

describe('InMemoryRepository', () => {
  let uuidGenerator: IUuidGenerator;
  let repository: ITodoRepository;

  beforeEach(() => {
    uuidGenerator = { generate: () => '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed' };
    repository = new InMemoryTodoRepository(uuidGenerator);
  });

  it('should get last todo order', async () => {
    let count = await repository.getLastOrder();
    expect(count).toBe(0);

    (repository as any).todos = [{ order: 1 }, { order: 2 }, { order: 3 }, { order: 4 }];

    count = await repository.getLastOrder();
    expect(count).toBe(4);
  });

  it('should create todo', async () => {
    const todo = new Todo('1', 'title', 1, false);
    const createdTodo = await repository.create({ title: todo.title, order: todo.order, completed: todo.completed });

    expect(createdTodo).toEqual({ id: '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed', title: 'title', order: 1, completed: false });
    expect((repository as any).todos).toHaveLength(1);
    expect((repository as any).todos[0]).toBe(createdTodo);
  });

  it('should delete todo', async () => {
    const todo = new Todo('1', 'title', 1, false);

    (repository as any).todos = [todo];

    const todoDeleted = await repository.delete('1');

    expect(todoDeleted).toBe(todo);
    expect((repository as any).todos).toHaveLength(0);
  });

  it('should not delete when not found', async () => {
    const todo = new Todo('1', 'title', 1, false);

    (repository as any).todos = [todo];

    const todoDeleted = await repository.delete('2');

    expect(todoDeleted).toBeUndefined();
    expect((repository as any).todos).toHaveLength(1);
    expect((repository as any).todos[0]).toBe(todo);
  });

  it.each`
    completed
    ${undefined}
    ${false}
  `('should delete all todos', async ({ completed }) => {
    ((repository as any).todos as Todo[]) = [
      { id: '1', title: 'title 1', completed: false, order: 1 },
      { id: '2', title: 'title 2', completed: false, order: 2 },
    ];

    completed === undefined ? await repository.deleteAll() : await repository.deleteAll(completed);

    expect((repository as any).todos).toHaveLength(0);
  });

  it('should delete all completed todos', async () => {
    ((repository as any).todos as Todo[]) = [
      { id: '1', title: 'title 1', completed: true, order: 1 },
      { id: '2', title: 'title 2', completed: false, order: 2 },
      { id: '3', title: 'title 3', completed: true, order: 3 },
    ];

    await repository.deleteAll(true);

    expect((repository as any).todos).toHaveLength(1);
    expect((repository as any).todos[0]).toEqual({ id: '2', title: 'title 2', completed: false, order: 2 });
  });

  it('should fetch todo', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetch('2');

    expect(fetchedTodo).toEqual(todo2);
  });

  it('should fetch undefined when no todo', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetch('3');

    expect(fetchedTodo).toBeUndefined();
  });

  it('should fetch todo by order', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetchByOrder(2);

    expect(fetchedTodo).toEqual(todo2);
  });

  it('should fetch undefined when invalid order', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetchByOrder(3);

    expect(fetchedTodo).toBeUndefined();
  });

  it('should fetch all todos', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const todos = await repository.fetchAll();

    expect((repository as any).todos).toBe(todos);
  });

  it('should fetch emtpy array when no todos', async () => {
    ((repository as any).todos as Todo[]) = [];

    const todos = await repository.fetchAll();

    expect(todos).toEqual([]);
  });

  it('should patch', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const patchedTodo = await repository.patch('1', { completed: false, order: 3, title: 'title 1 updated' });

    expect(patchedTodo).toEqual({ id: '1', title: 'title 1 updated', completed: false, order: 3, url: 'http://localhost/1' });
    expect((repository as any).todos[0]).toBe(patchedTodo);
    expect((repository as any).todos[1]).toBe(todo2);
  });

  it('should return undefined when patch not found', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const patchedTodo = await repository.patch('3', { completed: false, order: 3, title: 'title 1 updated' });

    expect(patchedTodo).toBeUndefined();
    expect((repository as any).todos[0]).toBe(todo1);
    expect((repository as any).todos[1]).toBe(todo2);
  });

  it('should not patch', async () => {
    const todo1 = { id: '1', title: 'title 1', completed: true, order: 1, url: 'http://localhost/1' };
    const todo2 = { id: '2', title: 'title 2', completed: true, order: 2, url: 'http://localhost/2' };
    ((repository as any).todos as Todo[]) = [todo1, todo2];

    const patchedTodo = await repository.patch('1', {});

    expect(patchedTodo).toEqual(todo1);
    expect((repository as any).todos[0]).toBe(patchedTodo);
    expect((repository as any).todos[1]).toBe(todo2);
  });
});
