import { ITodoRepository, TodoCreate, TodoPatch } from '~/src/application';
import { Todo } from '~/src/entities';
import { IUuidGenerator } from '~/src/infrastructure/repository';

export class InMemoryTodoRepository implements ITodoRepository {
  private todos: Todo[] = [];

  constructor(private uuidGenerator: IUuidGenerator) {}

  getLastOrder(): Promise<number> {
    const order = this.todos.length > 0 ? this.todos[this.todos.length - 1].order : 0;
    return Promise.resolve(order);
  }

  create(todoToSave: TodoCreate): Promise<Todo> {
    const todo = new Todo(this.uuidGenerator.generate(), todoToSave.title, todoToSave.order, todoToSave.completed);
    this.todos = [...this.todos, todo];
    return Promise.resolve(todo);
  }

  delete(id: string): Promise<Todo | undefined> {
    const todoToDelete = this.todos.find((t) => t.id === id);

    if (todoToDelete === undefined) {
      return Promise.resolve(undefined);
    }

    this.todos = this.todos.filter((todo) => todo.id !== todoToDelete.id);
    return Promise.resolve(todoToDelete);
  }

  deleteAll(completed = false): Promise<void> {
    if (completed === false) {
      this.todos = [];
    } else {
      this.todos = this.todos.filter((todo: Todo) => todo.completed !== completed);
    }

    return Promise.resolve();
  }

  fetch(id: string): Promise<Todo | undefined> {
    const todo = this.todos.find((todo: Todo) => todo.id === id);
    return Promise.resolve(todo);
  }

  fetchByOrder(order: number): Promise<Todo | undefined> {
    const todo = this.todos.find((todo: Todo) => todo.order === order);
    return Promise.resolve(todo);
  }

  fetchAll(): Promise<Todo[]> {
    return Promise.resolve(this.todos);
  }

  patch(id: string, { completed, order, title }: TodoPatch): Promise<Todo | undefined> {
    const index = this.todos.findIndex((todo: Todo) => todo.id === id);

    if (index === -1) {
      return Promise.resolve(undefined);
    }

    this.todos = this.todos.map((todo, i) => (i === index ? { ...todo, title: title ?? todo.title, completed: completed ?? todo.completed, order: order ?? todo.order } : todo));
    return Promise.resolve(this.todos[index]);
  }
}
