import { validate } from 'uuid';
import { IUuidGenerator, UuidGenerator } from '~/src/infrastructure/repository';

describe('UuidGenerator', () => {
  let uuidGenerator: IUuidGenerator;

  beforeEach(() => {
    uuidGenerator = new UuidGenerator();
  });

  it('should generate some uuids', () => {
    const uuid1 = uuidGenerator.generate();
    const uuid2 = uuidGenerator.generate();

    expect(validate(uuid1)).toBe(true);
    expect(validate(uuid2)).toBe(true);
    expect(uuid1).not.toBe(uuid2);
  });
});
