import { Request as ExpressRequest } from 'express';
import { IRequest } from '~/src/controllers';

export class Request implements IRequest {
  constructor(private readonly req: ExpressRequest) {}

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get headers(): any {
    return this.req.headers;
  }

  get protocol(): string {
    return this.req.protocol;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get body(): any {
    return this.req.body;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get query(): any {
    return this.req.query;
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  get params(): any {
    return this.req.params;
  }
}
