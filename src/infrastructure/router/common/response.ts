import { Response as ExpressResponse } from 'express';
import { HttpStatus, IResponse } from '~/src/controllers';

export class Response<T> implements IResponse<T> {
  constructor(private readonly res: ExpressResponse) {}

  send = (status: HttpStatus, body?: T | string): void => {
    body ? this.res.status(status).json(body) : this.res.sendStatus(status);
  };
}
