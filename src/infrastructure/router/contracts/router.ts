import { Router } from 'express';

export interface ITodoRouter {
  readonly router: Router;
}
