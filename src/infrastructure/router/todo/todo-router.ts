import { Request as ExpressRequest, Response as ExpressResponse, Router } from 'express';
import { IRequest, IResponse, ITodoController, TodoResponse } from '~/src/controllers';
import { ITodoRouter, Request, Response } from '~/src/infrastructure/router';

export class TodoRouter implements ITodoRouter {
  constructor(readonly router: Router, private readonly todoController: ITodoController) {
    this.router
      .route('/')
      .post(this.routeHandler((req: IRequest, res: IResponse<TodoResponse>) => this.todoController.addTodo(req, res)))
      .get(this.routeHandler((req: IRequest, res: IResponse<TodoResponse[]>) => this.todoController.getTodos(req, res)))
      .delete(this.routeHandler((req: IRequest, res: IResponse<void>) => this.todoController.deleteTodos(req, res)));

    this.router
      .route('/:id')
      .get(this.routeHandler((req: IRequest, res: IResponse<TodoResponse>) => this.todoController.getTodo(req, res)))
      .patch(this.routeHandler((req: IRequest, res: IResponse<TodoResponse>) => this.todoController.patchTodo(req, res)))
      .delete(this.routeHandler((req: IRequest, res: IResponse<TodoResponse>) => this.todoController.deleteTodo(req, res)));
  }

  private routeHandler<T>(fn: (req: IRequest, res: IResponse<T>) => Promise<void>) {
    return (req: ExpressRequest, res: ExpressResponse) => {
      const request = new Request(req);
      const response = new Response<T>(res);
      return fn(request, response);
    };
  }
}
