import express, { Router } from 'express';
import { ITodoController } from '~/src/controllers';
import { ITodoRouter, TodoRouter } from '~/src/infrastructure/router';

describe('TodoRouter', () => {
  let router: Router;
  let controller: ITodoController;
  let todoRouter: ITodoRouter;

  beforeEach(() => {
    router = express.Router();
    controller = {
      addTodo: jest.fn(),
      deleteTodo: jest.fn(),
      deleteTodos: jest.fn(),
      getTodo: jest.fn(),
      getTodos: jest.fn(),
      patchTodo: jest.fn(),
    };
    todoRouter = new TodoRouter(router, controller);
  });

  it.each`
    path      | method      | methodToBeCalled
    ${'/'}    | ${'post'}   | ${'addTodo'}
    ${'/'}    | ${'get'}    | ${'getTodos'}
    ${'/'}    | ${'delete'} | ${'deleteTodos'}
    ${'/:id'} | ${'get'}    | ${'getTodo'}
    ${'/:id'} | ${'patch'}  | ${'patchTodo'}
    ${'/:id'} | ${'delete'} | ${'deleteTodo'}
  `('should call controller method', ({ path, method, methodToBeCalled }: { path: string; method: string; methodToBeCalled: keyof ITodoController }) => {
    const handle = todoRouter.router.stack.find((s) => s.route.path === path).route.stack.find((s: { method: string }) => s.method === method).handle;

    handle();

    const properties = Object.getOwnPropertyNames(controller);
    expect(properties).toHaveLength(6);

    let countMethodCalled = 0;
    let countOtherMethods = 0;

    properties.forEach((key) => {
      if (key === methodToBeCalled) {
        expect(controller[key]).toHaveBeenCalledTimes(1);
        countMethodCalled++;
      } else {
        expect(controller[key as keyof ITodoController]).not.toHaveBeenCalled();
        countOtherMethods++;
      }
    });

    expect(countMethodCalled).toBe(1);
    expect(countOtherMethods).toBe(5);
  });
});
