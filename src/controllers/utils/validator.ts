import { plainToClass } from 'class-transformer';
import { validate, ValidationError } from 'class-validator';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type ClassConstructor<T> = { new (...args: any[]): T };

type Validate<T> = { validatedObject: T; errors: ValidationError[] };

// eslint-disable-next-line @typescript-eslint/ban-types
export async function validateInputs<T extends object>(type: ClassConstructor<T>, inputs: unknown): Promise<Validate<T>> {
  const objToValidate = plainToClass(type, inputs);
  const errors = await validate(objToValidate);
  return { validatedObject: objToValidate, errors };
}
