import { Todo } from '~/src/entities';

export type TodoResponse = Todo & { url: string };
