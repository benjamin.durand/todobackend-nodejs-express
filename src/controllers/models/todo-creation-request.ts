import { IsString } from 'class-validator';
import { IsNotBlank } from '../utils/isNotBlank';

export class TodoCreationRequest {
  @IsNotBlank()
  @IsString()
  title!: string;
}
