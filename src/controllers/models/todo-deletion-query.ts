import { Transform } from 'class-transformer';
import { IsBoolean, IsOptional } from 'class-validator';

export class TodoDeletionQuery {
  @Transform(({ value }) => {
    if (typeof value !== 'boolean' && value !== 'true' && value !== 'false') {
      return -1;
    }
    return [true, 'true'].indexOf(value) > -1;
  })
  @IsBoolean()
  @IsOptional()
  completed = false;
}
