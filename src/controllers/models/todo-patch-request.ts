import { Transform, Type } from 'class-transformer';
import { IsBoolean, IsNumber, IsOptional, IsString, Min } from 'class-validator';
import { IsNotBlank } from '../utils/isNotBlank';

export class TodoPatchRequest {
  @IsNotBlank()
  @IsString()
  @IsOptional()
  title?: string;

  @Type(() => Number)
  @Min(1)
  @IsNumber()
  @IsOptional()
  order?: number;

  @Transform(({ value }) => {
    if (typeof value !== 'boolean' && value !== 'true' && value !== 'false') {
      return -1;
    }
    return [true, 'true'].indexOf(value) > -1;
  })
  @IsBoolean()
  @IsOptional()
  completed?: boolean;
}
