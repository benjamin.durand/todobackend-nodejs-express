import { Type } from 'class-transformer';
import { IsString } from 'class-validator';

export class TodoIdParams {
  @Type(() => String)
  @IsString()
  id!: string;
}
