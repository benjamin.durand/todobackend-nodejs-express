export interface IRequest {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  headers: any;
  protocol: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  body: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  query: any;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  params: any;
}
