import { IRequest, IResponse, TodoResponse } from '~/src/controllers';

export interface ITodoController {
  addTodo(request: IRequest, response: IResponse<TodoResponse>): Promise<void>;

  deleteTodo(request: IRequest, response: IResponse<TodoResponse>): Promise<void>;

  deleteTodos(request: IRequest, response: IResponse<void>): Promise<void>;

  getTodo(request: IRequest, response: IResponse<TodoResponse>): Promise<void>;

  getTodos(request: IRequest, response: IResponse<TodoResponse[]>): Promise<void>;

  patchTodo(request: IRequest, response: IResponse<TodoResponse>): Promise<void>;
}
