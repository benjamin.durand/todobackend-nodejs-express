import { ValidationError } from 'class-validator';
import { ConflictingOrderException, ITodoUseCases } from '~/src/application';
import {
  HttpStatus,
  IRequest,
  IResponse,
  ITodoController,
  TodoCreationRequest,
  TodoDeletionQuery,
  TodoIdParams,
  TodoPatchRequest,
  TodoResponse,
  validateInputs,
} from '~/src/controllers';

export class TodoController implements ITodoController {
  constructor(private readonly todoUseCases: ITodoUseCases) {}

  async addTodo(req: IRequest, { send }: IResponse<TodoResponse>): Promise<void> {
    const { validatedObject, errors } = await validateInputs(TodoCreationRequest, req.body);
    if (errors.length > 0) {
      send(HttpStatus.BAD_REQUEST, this.getAddTodoError(errors));
      return;
    }

    try {
      const todo = await this.todoUseCases.createTodoUseCase.exec(validatedObject.title);
      send(HttpStatus.CREATED, { ...todo, url: this.generateTodoUrl(req, todo.id) });
    } catch (error: unknown) {
      send(HttpStatus.INTERNAL_SERVER_ERROR, this.getError(error));
    }
  }

  async deleteTodo({ params }: IRequest, { send }: IResponse<TodoResponse>): Promise<void> {
    const { validatedObject, errors } = await validateInputs(TodoIdParams, params);
    if (errors.length > 0) {
      send(HttpStatus.BAD_REQUEST, this.getTodoIdError(errors));
      return;
    }

    try {
      const todo = await this.todoUseCases.deleteTodoUseCase.exec(validatedObject.id);
      if (todo) {
        send(HttpStatus.NO_CONTENT);
      } else {
        send(HttpStatus.NOT_FOUND);
      }
    } catch (error: unknown) {
      send(HttpStatus.INTERNAL_SERVER_ERROR, this.getError(error));
    }
  }

  async deleteTodos({ query }: IRequest, { send }: IResponse<void>): Promise<void> {
    const { validatedObject, errors } = await validateInputs(TodoDeletionQuery, query);
    if (errors.length > 0) {
      send(HttpStatus.BAD_REQUEST, this.getTodoDeletionError(errors));
      return;
    }

    try {
      await this.todoUseCases.deleteAllTodoUseCase.exec(validatedObject.completed);
      send(HttpStatus.NO_CONTENT);
    } catch (error: unknown) {
      send(HttpStatus.INTERNAL_SERVER_ERROR, this.getError(error));
    }
  }

  async getTodo(req: IRequest, { send }: IResponse<TodoResponse>): Promise<void> {
    const { validatedObject, errors } = await validateInputs(TodoIdParams, req.params);
    if (errors.length > 0) {
      send(HttpStatus.BAD_REQUEST, this.getTodoIdError(errors));
      return;
    }

    try {
      const todo = await this.todoUseCases.fetchTodoUseCase.exec(validatedObject.id);
      if (todo) {
        send(HttpStatus.OK, { ...todo, url: this.generateTodoUrl(req, todo.id) });
      } else {
        send(HttpStatus.NOT_FOUND);
      }
    } catch (error: unknown) {
      send(HttpStatus.INTERNAL_SERVER_ERROR, this.getError(error));
    }
  }

  async getTodos(req: IRequest, { send }: IResponse<TodoResponse[]>): Promise<void> {
    try {
      const todos = await this.todoUseCases.fetchAllTodoUseCase.exec();
      send(
        HttpStatus.OK,
        todos.map((todo) => ({ ...todo, url: this.generateTodoUrl(req, todo.id) })),
      );
    } catch (error: unknown) {
      send(HttpStatus.INTERNAL_SERVER_ERROR, this.getError(error));
    }
  }

  async patchTodo(req: IRequest, { send }: IResponse<TodoResponse>): Promise<void> {
    const { validatedObject: validatedParams, errors: paramsErrors } = await validateInputs(TodoIdParams, req.params);
    if (paramsErrors.length > 0) {
      send(HttpStatus.BAD_REQUEST, this.getTodoIdError(paramsErrors));
      return;
    }

    const { validatedObject: validatedBody, errors: bodyErrors } = await validateInputs(TodoPatchRequest, req.body);
    if (bodyErrors.length > 0) {
      send(HttpStatus.BAD_REQUEST, this.getUpdateTodoError(bodyErrors));
      return;
    }

    try {
      const todo = await this.todoUseCases.patchTodoUseCase.exec(validatedParams.id, validatedBody);
      if (todo) {
        send(HttpStatus.OK, { ...todo, url: this.generateTodoUrl(req, todo.id) });
      } else {
        send(HttpStatus.NOT_FOUND);
      }
    } catch (error: unknown) {
      if (error instanceof ConflictingOrderException) {
        send(HttpStatus.CONFLICT, error.message);
      } else {
        send(HttpStatus.INTERNAL_SERVER_ERROR, this.getError(error));
      }
    }
  }

  private getAddTodoError(errors: ValidationError[]): string {
    let error = '';

    const constraints = errors[0].constraints;
    if (constraints?.isString) {
      error = constraints.isString;
    } else if (constraints?.isNotBlank) {
      error = constraints.isNotBlank;
    }

    return error;
  }

  private getTodoIdError(errors: ValidationError[]): string {
    let error = '';

    const constraints = errors[0].constraints;
    if (constraints?.isString) {
      error = constraints.isString;
    }

    return error;
  }

  private getTodoDeletionError(errors: ValidationError[]): string {
    let error = '';

    const constraints = errors[0].constraints;
    if (constraints?.isBoolean) {
      error = constraints.isBoolean;
    }

    return error;
  }

  private getUpdateTodoError(errors: ValidationError[]): string {
    let error = '';

    for (const err of errors) {
      if (err.property === 'title') {
        if (err.constraints?.isString) {
          error = err.constraints.isString;
        } else if (err.constraints?.isNotBlank) {
          error = err.constraints.isNotBlank;
        }
      } else if (err.property === 'order') {
        const constraints = err.constraints;
        if (constraints?.isNumber) {
          error = constraints.isNumber;
        } else if (constraints?.min) {
          error = constraints.min;
        }
      } else if (err.property === 'completed') {
        const constraints = err.constraints;
        if (constraints?.isBoolean) {
          error = constraints.isBoolean;
        }
      }

      if (error) {
        break;
      }
    }

    return error;
  }

  private getError(error: unknown) {
    if (error instanceof Error) {
      return error.message;
    }
    return typeof error === 'string' ? error : undefined;
  }

  private generateTodoUrl({ protocol, headers }: IRequest, id: string): string {
    return `${protocol}://${headers.host}/todos/${id}`;
  }
}
