import { ConflictingOrderException, ITodoUseCases } from '~/src/application';
import { HttpStatus, IRequest, IResponse, ITodoController, TodoController } from '~/src/controllers';
import { Todo } from '~/src/entities';

describe('TodoController', () => {
  let todoController: ITodoController;
  let useCases: ITodoUseCases;
  let request: IRequest;
  let response: IResponse<unknown>;

  beforeEach(() => {
    useCases = {
      createTodoUseCase: { exec: jest.fn() },
      deleteTodoUseCase: { exec: jest.fn() },
      deleteAllTodoUseCase: { exec: jest.fn() },
      fetchTodoUseCase: { exec: jest.fn() },
      fetchAllTodoUseCase: { exec: jest.fn() },
      patchTodoUseCase: { exec: jest.fn() },
    };

    todoController = new TodoController(useCases);

    response = {
      send: jest.fn(),
    };

    request = { headers: { host: 'localhost' }, protocol: 'http', body: null, params: null, query: null };
  });

  describe('addTodo', () => {
    it('should send a created todo', async () => {
      const todo = new Todo('1', 'a title', 1, false);
      const execSpy = jest.spyOn(useCases.createTodoUseCase, 'exec').mockReturnValue(Promise.resolve(todo));
      const sendSpy = jest.spyOn(response, 'send');

      const body = { title: 'a title' };
      request = { ...request, body };

      const result = await todoController.addTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(body.title);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.CREATED, { ...todo, url: 'http://localhost/todos/1' });

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it.each`
      title        | errorMsg
      ${null}      | ${'title must be a string'}
      ${undefined} | ${'title must be a string'}
      ${1}         | ${'title must be a string'}
      ${true}      | ${'title must be a string'}
      ${{}}        | ${'title must be a string'}
      ${[]}        | ${'title must be a string'}
      ${''}        | ${'title must not be blank'}
      ${' '}       | ${'title must not be blank'}
    `('should send bad request when invalid title', async ({ title, errorMsg }: { title: string; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.createTodoUseCase, 'exec');
      const sendSpy = jest.spyOn(response, 'send');

      const body = { title };
      request = { ...request, body };

      const result = await todoController.addTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it('should send internal server error when use case rejects', async () => {
      const execSpy = jest.spyOn(useCases.createTodoUseCase, 'exec').mockReturnValue(Promise.reject('An error occurred'));
      const sendSpy = jest.spyOn(response, 'send');

      const body = { title: 'a title' };
      request = { ...request, body };

      const result = await todoController.addTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(body.title);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR, 'An error occurred');

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });
  });

  describe('deleteTodo', () => {
    it('should send no content', async () => {
      const todo = new Todo('1', 'a title', 1, false);
      const execSpy = jest.spyOn(useCases.deleteTodoUseCase, 'exec').mockReturnValue(Promise.resolve(todo));
      const sendSpy = jest.spyOn(response, 'send');

      const params = { id: 1 };
      request = { ...request, params };

      const result = await todoController.deleteTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString());

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.NO_CONTENT);

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it('should send not found', async () => {
      const execSpy = jest.spyOn(useCases.deleteTodoUseCase, 'exec').mockReturnValue(Promise.resolve(undefined));
      const sendSpy = jest.spyOn(response, 'send');

      const params = { id: 1 };
      request = { ...request, params };

      const result = await todoController.deleteTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString());

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it.each`
      id           | errorMsg
      ${null}      | ${'id must be a string'}
      ${undefined} | ${'id must be a string'}
      ${[]}        | ${'id must be a string'}
    `('should send bad request when invalid id', async ({ id, errorMsg }: { id: number; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.deleteTodoUseCase, 'exec');

      const params = { id };
      request = { ...request, params };

      const result = await todoController.deleteTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it('should send internal server error when use case rejects', async () => {
      const execSpy = jest.spyOn(useCases.deleteTodoUseCase, 'exec').mockReturnValue(Promise.reject(new Error("Can't process usecase")));
      const sendSpy = jest.spyOn(response, 'send');

      const params = { id: 1 };
      request = { ...request, params };

      const result = await todoController.deleteTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString());

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR, "Can't process usecase");

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });
  });

  describe('deleteTodos', () => {
    it.each`
      completed  | calledWith
      ${false}   | ${false}
      ${'false'} | ${false}
      ${true}    | ${true}
      ${'true'}  | ${true}
    `('should send no content', async ({ completed, calledWith }: { completed: boolean; calledWith: boolean }) => {
      const execSpy = jest.spyOn(useCases.deleteAllTodoUseCase, 'exec').mockReturnValue(Promise.resolve());
      const sendSpy = jest.spyOn(response, 'send');

      const query = { completed };
      request = { ...request, query };

      const result = await todoController.deleteTodos(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(calledWith);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.NO_CONTENT);

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it.each`
      completed    | errorMsg
      ${undefined} | ${'completed must be a boolean value'}
      ${null}      | ${'completed must be a boolean value'}
      ${{}}        | ${'completed must be a boolean value'}
      ${[]}        | ${'completed must be a boolean value'}
      ${''}        | ${'completed must be a boolean value'}
      ${-1}        | ${'completed must be a boolean value'}
      ${0}         | ${'completed must be a boolean value'}
    `('should send bad request when invalid completed', async ({ completed, errorMsg }: { completed: boolean; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.deleteAllTodoUseCase, 'exec');

      const query = { completed };
      request = { ...request, query };

      const result = await todoController.deleteTodos(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it('should send internal server error when use case rejects', async () => {
      const execSpy = jest.spyOn(useCases.deleteAllTodoUseCase, 'exec').mockReturnValue(Promise.reject(new Error("Can't process usecase")));
      const sendSpy = jest.spyOn(response, 'send');

      const query = { completed: false };
      request = { ...request, query };

      const result = await todoController.deleteTodos(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(query.completed);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR, "Can't process usecase");

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });
  });

  describe('getTodo', () => {
    it('should return a fetched todo', async () => {
      const todo = new Todo('1', 'a title', 1, false);
      const execSpy = jest.spyOn(useCases.fetchTodoUseCase, 'exec').mockReturnValue(Promise.resolve(todo));
      const sendSpy = jest.spyOn(response, 'send');

      const params = { id: 1 };
      request = { ...request, params };

      const result = await todoController.getTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString());

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.OK, { ...todo, url: 'http://localhost/todos/1' });

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it('should send not found', async () => {
      const execSpy = jest.spyOn(useCases.fetchTodoUseCase, 'exec').mockReturnValue(Promise.resolve(undefined));
      const sendSpy = jest.spyOn(response, 'send');

      const params = { id: 1 };
      request = { ...request, params };

      const result = await todoController.getTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString());

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it.each`
      id           | errorMsg
      ${null}      | ${'id must be a string'}
      ${undefined} | ${'id must be a string'}
      ${[]}        | ${'id must be a string'}
    `('should send bad request when invalid id', async ({ id, errorMsg }: { id: number; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.fetchTodoUseCase, 'exec');

      const params = { id };
      request = { ...request, params };

      const result = await todoController.getTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it('should send internal server error when use case rejects', async () => {
      const execSpy = jest.spyOn(useCases.fetchTodoUseCase, 'exec').mockReturnValue(Promise.reject(new Error("Can't process usecase")));
      const sendSpy = jest.spyOn(response, 'send');

      const params = { id: 1 };
      request = { ...request, params };

      const result = await todoController.getTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString());

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR, "Can't process usecase");

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });
  });

  describe('getTodos', () => {
    it('should return fetched todos', async () => {
      const todos = [new Todo('1', 'a title', 1, false), new Todo('2', 'another title', 2, false)];
      const execSpy = jest.spyOn(useCases.fetchAllTodoUseCase, 'exec').mockReturnValue(Promise.resolve(todos));
      const sendSpy = jest.spyOn(response, 'send');

      const result = await todoController.getTodos(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith();

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(
        HttpStatus.OK,
        todos.map((todo) => ({ ...todo, url: `http://localhost/todos/${todo.id}` })),
      );

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it('should send internal server error when use case rejects', async () => {
      const execSpy = jest.spyOn(useCases.fetchAllTodoUseCase, 'exec').mockReturnValue(Promise.reject(new Error("Can't process usecase")));
      const sendSpy = jest.spyOn(response, 'send');

      const result = await todoController.getTodos(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith();

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR, "Can't process usecase");

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });
  });

  describe('patchTodo', () => {
    it('should return patched todo', async () => {
      const todo = new Todo('1', 'a title', 1, false);
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec').mockReturnValue(Promise.resolve(todo));
      const sendSpy = jest.spyOn(response, 'send');

      const body = { title: 'another title', order: 2, completed: true };
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString(), body);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.OK, { ...todo, url: 'http://localhost/todos/1' });

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it('should return patched todo with plain body', async () => {
      const todo = new Todo('1', 'a title', 1, false);
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec').mockReturnValue(Promise.resolve(todo));
      const sendSpy = jest.spyOn(response, 'send');

      const body = {};
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString(), body);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.OK, { ...todo, url: 'http://localhost/todos/1' });

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it('should send not found when no todo', async () => {
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec').mockReturnValue(Promise.resolve(undefined));
      const sendSpy = jest.spyOn(response, 'send');

      const body = { title: 'another title', order: 2, completed: true };
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString(), body);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.NOT_FOUND);

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it.each`
      id           | errorMsg
      ${null}      | ${'id must be a string'}
      ${undefined} | ${'id must be a string'}
      ${[]}        | ${'id must be a string'}
    `('should send bad request when invalid id', async ({ id, errorMsg }: { id: number; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec');

      const body = { title: 'another title', order: 2, completed: true };
      const params = { id };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it.each`
      title   | errorMsg
      ${1}    | ${'title must be a string'}
      ${true} | ${'title must be a string'}
      ${{}}   | ${'title must be a string'}
      ${[]}   | ${'title must be a string'}
      ${''}   | ${'title must not be blank'}
      ${' '}  | ${'title must not be blank'}
    `('should send bad request when invalid title', async ({ title, errorMsg }: { title: string; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec');

      const body = { title };
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it.each`
      order | errorMsg
      ${{}} | ${'order must be a number conforming to the specified constraints'}
      ${[]} | ${'order must be a number conforming to the specified constraints'}
      ${''} | ${'order must not be less than 1'}
      ${-1} | ${'order must not be less than 1'}
      ${0}  | ${'order must not be less than 1'}
    `('should send bad request when invalid order', async ({ order, errorMsg }: { order: number; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec');

      const body = { order };
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it.each`
      completed    | errorMsg
      ${undefined} | ${'completed must be a boolean value'}
      ${null}      | ${'completed must be a boolean value'}
      ${{}}        | ${'completed must be a boolean value'}
      ${[]}        | ${'completed must be a boolean value'}
      ${''}        | ${'completed must be a boolean value'}
      ${-1}        | ${'completed must be a boolean value'}
      ${0}         | ${'completed must be a boolean value'}
    `('should send bad request when invalid completed', async ({ completed, errorMsg }: { completed: boolean; errorMsg: string }) => {
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec');

      const body = { completed };
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).not.toHaveBeenCalled();

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.BAD_REQUEST, errorMsg);
    });

    it('should send conflict when conflicting order exception thrown', async () => {
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec').mockReturnValue(Promise.reject(new ConflictingOrderException('Conflicting order occurred')));
      const sendSpy = jest.spyOn(response, 'send');

      const body = { title: 'another title', order: 2, completed: false };
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString(), body);

      expect(sendSpy).toHaveBeenCalledTimes(1);
      expect(sendSpy).toHaveBeenCalledWith(HttpStatus.CONFLICT, 'Conflicting order occurred');

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });

    it('should send internal server error when use case rejects', async () => {
      const execSpy = jest.spyOn(useCases.patchTodoUseCase, 'exec').mockReturnValue(Promise.reject(new Error("Can't process usecase")));
      const sendSpy = jest.spyOn(response, 'send');

      const body = { title: 'another title' };
      const params = { id: 1 };
      request = { ...request, body, params };

      const result = await todoController.patchTodo(request, response);

      expect(result).toBeUndefined();

      expect(execSpy).toHaveBeenCalledTimes(1);
      expect(execSpy).toHaveBeenCalledWith(params.id.toString(), body);

      expect(response.send).toHaveBeenCalledTimes(1);
      expect(response.send).toHaveBeenCalledWith(HttpStatus.INTERNAL_SERVER_ERROR, "Can't process usecase");

      expect(execSpy.mock.invocationCallOrder[0]).toBeLessThan(sendSpy.mock.invocationCallOrder[0]);
    });
  });
});
