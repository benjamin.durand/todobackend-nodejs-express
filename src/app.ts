import 'reflect-metadata';

import { config } from 'dotenv';
import express from 'express';
import { container } from '~/src/config';

config();

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());

app.use('/todos', container.cradle.todoRouter.router);

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
