import { asClass, asFunction, createContainer, InjectionMode } from 'awilix';
import express, { Router } from 'express';
import {
  CreateTodoUseCase,
  DeleteTodosUseCase,
  DeleteTodoUseCase,
  FetchTodosUseCase,
  FetchTodoUseCase,
  ICreateTodoUseCase,
  IDeleteTodosUseCase,
  IDeleteTodoUseCase,
  IFetchTodosUseCase,
  IFetchTodoUseCase,
  IPatchTodoUseCase,
  ITodoRepository,
  ITodoUseCases,
  PatchTodoUseCase,
  TodoUseCases,
} from '~/src/application';
import { ITodoController, TodoController } from '~/src/controllers';
import { InMemoryTodoRepository, IUuidGenerator, UuidGenerator } from '~/src/infrastructure/repository';
import { ITodoRouter, TodoRouter } from '~/src/infrastructure/router';

interface ICradle {
  uuidGenerator: IUuidGenerator;
  todoRepository: ITodoRepository;

  createTodoUseCase: ICreateTodoUseCase;
  deleteTodoUseCase: IDeleteTodoUseCase;
  deleteAllTodoUseCase: IDeleteTodosUseCase;
  fetchTodoUseCase: IFetchTodoUseCase;
  fetchAllTodoUseCase: IFetchTodosUseCase;
  patchTodoUseCase: IPatchTodoUseCase;

  todoUseCases: ITodoUseCases;

  todoController: ITodoController;

  todoRouter: ITodoRouter;
  router: Router;
}

export const container = createContainer<ICradle>({
  injectionMode: InjectionMode.CLASSIC,
});

container.register({
  uuidGenerator: asClass(UuidGenerator).singleton(),
  todoRepository: asClass(InMemoryTodoRepository).singleton(),

  createTodoUseCase: asClass(CreateTodoUseCase),
  deleteTodoUseCase: asClass(DeleteTodoUseCase),
  deleteAllTodoUseCase: asClass(DeleteTodosUseCase),
  fetchTodoUseCase: asClass(FetchTodoUseCase),
  fetchAllTodoUseCase: asClass(FetchTodosUseCase),
  patchTodoUseCase: asClass(PatchTodoUseCase),

  todoUseCases: asClass(TodoUseCases),

  todoController: asClass(TodoController),

  todoRouter: asClass(TodoRouter),
  router: asFunction(() => express.Router()),
});
